QUnit.test('Testing calculateSimple function with several sets of inputs', function (assert) {
    assert.equal(calculateSimple(100,5,1), 105, 'valid arguments returns total amount');
    assert.equal(calculateSimple(-1,5,1), 0, 'invalid arguments returns 0');
    assert.equal(calculateSimple('','',''), 0, 'invalid arguments returns 0');
    assert.equal(calculateSimple('abc',5,1), 0, 'invalid arguments returns 0');
    
  });